import {ArithmeticModule} from '../../src/modules/arithmetic.module';
import { inject, TestBed } from '@angular/core/testing';
import { DataService} from './../../angular-library-starter';
import { HttpClient } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { Configuration } from '../../src/utils/constants';
describe('DataService', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DataService,
                HttpClient,
                HttpHandler,
                Configuration
            ]
        });
    });

    it('Get all data from service',
        inject([DataService],
            (dataService: DataService) => {
                dataService.getAll<any[]>();
            })
    );

});
