import {DataService} from '../services/Post.service';
import {Configuration} from '../utils/constants';
import { HttpClient } from "@angular/common/http";

export function DataFactory(httpClient: HttpClient, configuration: Configuration) {
  return new DataService(httpClient, configuration);
}
