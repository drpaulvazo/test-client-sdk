import {Configuration} from '../utils/constants';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SumService } from '../services/sum.service';
import {DataService} from '../services/Post.service';
import { DataFactory } from '../index';
import { HttpClient } from '@angular/common/http';

@NgModule({
    declarations: [
        // Pipes.
        // Directives.
    ],
    exports: [
        // Pipes.
        // Directives.
    ],
    imports: [
        HttpClientModule
    ],
    providers: [
        {
        provide: DataService,
        useFactory: DataFactory,
        // tslint:disable-next-line:object-literal-sort-keys
        deps: [HttpClient, Configuration]
        }
    ]
})
export class ArithmeticModule {

    /**
     * Use in AppModule: new instance of SumService.
     */
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: ArithmeticModule,
            providers: [
                Configuration, {
                provide: DataService,
                useFactory: DataFactory,
                // tslint:disable-next-line:object-literal-sort-keys
                deps: [HttpClient, Configuration]
                }]
        };
    }

    /**
     * Use in features modules with lazy loading: new instance of SumService.
     */
    public static forChild(): ModuleWithProviders {
        return {
            ngModule: ArithmeticModule,
            providers: [
                Configuration,
                {
                provide: DataService,
                useFactory: DataFactory,
                // tslint:disable-next-line:object-literal-sort-keys
                deps: [HttpClient, Configuration]
                }]
        };
    }

}
