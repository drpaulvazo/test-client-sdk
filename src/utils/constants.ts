import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public Server = 'https://api.github.com/';
    public ApiUrl = 'users/';
    public ServerWithApiUrl = this.Server + this.ApiUrl;
}