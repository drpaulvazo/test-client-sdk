"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Public classes.
var sum_service_1 = require("./services/sum.service");
exports.SumService = sum_service_1.SumService;
var arithmetic_module_1 = require("./modules/arithmetic.module");
exports.ArithmeticModule = arithmetic_module_1.ArithmeticModule;
var Post_service_1 = require("./services/Post.service");
exports.DataService = Post_service_1.DataService;
