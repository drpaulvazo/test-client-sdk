"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../utils/constants");
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var Post_service_1 = require("../services/Post.service");
var index_1 = require("../index");
var http_2 = require("@angular/common/http");
var ArithmeticModule = /** @class */ (function () {
    function ArithmeticModule() {
    }
    ArithmeticModule_1 = ArithmeticModule;
    /**
     * Use in AppModule: new instance of SumService.
     */
    ArithmeticModule.forRoot = function () {
        return {
            ngModule: ArithmeticModule_1,
            providers: [
                constants_1.Configuration, {
                    provide: Post_service_1.DataService,
                    useFactory: index_1.DataFactory,
                    // tslint:disable-next-line:object-literal-sort-keys
                    deps: [http_2.HttpClient, constants_1.Configuration]
                }
            ]
        };
    };
    /**
     * Use in features modules with lazy loading: new instance of SumService.
     */
    ArithmeticModule.forChild = function () {
        return {
            ngModule: ArithmeticModule_1,
            providers: [
                constants_1.Configuration,
                {
                    provide: Post_service_1.DataService,
                    useFactory: index_1.DataFactory,
                    // tslint:disable-next-line:object-literal-sort-keys
                    deps: [http_2.HttpClient, constants_1.Configuration]
                }
            ]
        };
    };
    ArithmeticModule = ArithmeticModule_1 = __decorate([
        core_1.NgModule({
            declarations: [],
            exports: [],
            imports: [
                http_1.HttpClientModule
            ],
            providers: [
                {
                    provide: Post_service_1.DataService,
                    useFactory: index_1.DataFactory,
                    // tslint:disable-next-line:object-literal-sort-keys
                    deps: [http_2.HttpClient, constants_1.Configuration]
                }
            ]
        })
    ], ArithmeticModule);
    return ArithmeticModule;
    var ArithmeticModule_1;
}());
exports.ArithmeticModule = ArithmeticModule;
