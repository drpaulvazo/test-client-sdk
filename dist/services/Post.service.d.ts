import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../utils/constants';
export declare class DataService {
    private http;
    private configuration;
    private actionUrl;
    constructor(http: HttpClient, configuration: Configuration);
    getAll<T>(): Observable<T>;
    getSingle<T>(id: number): Observable<T>;
    add<T>(itemName: string): Observable<T>;
    update<T>(id: number, itemToUpdate: any): Observable<T>;
    delete<T>(id: number): Observable<T>;
}
