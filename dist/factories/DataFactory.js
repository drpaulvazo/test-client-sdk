"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Post_service_1 = require("../services/Post.service");
function DataFactory(httpClient, configuration) {
    return new Post_service_1.DataService(httpClient, configuration);
}
exports.DataFactory = DataFactory;
