import { DataService } from '../services/Post.service';
import { Configuration } from '../utils/constants';
import { HttpClient } from "@angular/common/http";
export declare function DataFactory(httpClient: HttpClient, configuration: Configuration): DataService;
